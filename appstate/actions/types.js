// TYPES FOR COMMON...
export const ATTEMPT = 'attempt';
export const SUCCESS = 'success';
export const FAIL = 'fail';
export const CLEAR = 'clear';
export const EDIT = 'edit';
export const MODAL = 'modal';


//TYPES FOR REMINDER
export const REMINDER_FETCH = 'reminder_fetch';

// TYPES FOR AUTH...
export const LOGIN = 'login';

// TYPES FOR PATIENT...
export const PATIENT_FETCH = 'patient_fetch';

// TYPES FOR DIAGNOSIS...
export const DIAGNOSIS_FETCH = 'diagnosis_fetch';

// TYPES FOR MEDICATONS... 
export const MEDICATION_FETCH = 'medication_fetch_success';

// TYPES FOR DOCTOR... 
export const DOCTOR_FETCH = 'doctor_fetch_success';

// TYPES FOR PROFILE
export const PROFILE_FETCH = 'profile_fetch';

// TYPES FOR SUMMARY
export const SUMMARY_FETCH = 'summary_fetch';

// TYPES FOR MESSAGES
export const CHATS_FETCH = 'chats_fetch';

// TYPE FOR AUDIO MESSAGE
export const CHATS_AUDIO = 'chats_audio';